# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:login_page_action.py
# @software:PyCharm
# 登录页面的元素及操作
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from page_locators.login_page_ele_locator import LoginPageLocator as loc
from base.base_page import BasePage
import time


class LoginPage(BasePage):

    def login_page(self, name, password):
        doc = '登录页面)_正常登录'
        self.wait_ele_visible(loc.username_input, doc=doc)
        ele_name = self.driver.find_element(*loc.username_input)
        ele_name.send_keys(name)
        ele_password = self.driver.find_element(*loc.password_input)
        ele_password.send_keys(password)
        time.sleep(2)
        ele_bt = self.driver.find_element(*loc.login_bt)
        ele_bt.click()

    # 注册的入口
    def register_enter(self):
        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(loc.register_bt))
        self.driver.find_element(*loc.register_bt).click()

    # 忘记密码入口
    def forget_pw(self):
        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(loc.forget_pw_bt))
        self.driver.find_element(*loc.forget_pw_bt).click()

    # 获取错误提示信息 -登录区域
    def get_err_mg_from_lg_area(self):
        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(loc.message_err))
        message_err_ele = self.driver.find_element(*loc.message_err)
        print(message_err_ele.text)
        return message_err_ele.text

    # 点击打开的弹框
    def click_open_alert(self):
        try:
            WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(loc.language_english))
            self.driver.find_element(*loc.language_english).click()
        except Exception as e:
            print(e)
            raise e

    # # 获取错误提示信息 -页面区域
    # def get_err_mg_from_center(self):
    #     WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(loc.mg_err))
    #     message_err_ele = self.driver.find_element(*loc.mg_err)
    #     return message_err_ele.text
