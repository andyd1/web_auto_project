# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:conftest.py
# @software:PyCharm
# 前置条件和后置条件的封装
import pytest
from selenium import webdriver
from datas import login_data as ld
from datas import common_data as cd


@pytest.fixture(scope='class')
def web_access():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(cd.url)
    login_data = ld.login_fail_data
    yield driver, login_data
    # driver.quit()
    pass

