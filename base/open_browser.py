# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:open_browser.py
# @software:PyCharm
# 打开不同浏览器的封装
from selenium import webdriver
from datas import common_data as cd
from base.logger import Logger


class OpenBrowser:
    def __init__(self, browser):
        self.browser = browser

    def open_browser(self):
        if self.browser.lower() == 'chrome':
            driver = webdriver.Chrome()
            driver.maximize_window()
            driver.get(cd.url)
            return driver

        elif self.browser.lower() == 'firefox':
            driver = webdriver.Firefox()
            driver.maximize_window()
            driver.get(cd.url)
            return driver

        elif self.browser.lower() == 'ie':
            driver = webdriver.Ie()
            driver.maximize_window()
            driver.get(cd.url)
            return driver

        elif self.browser.lower() == 'edge':
            driver = webdriver.Edge()
            driver.maximize_window()
            driver.get(cd.url)
            return driver

        elif self.browser.lower() == 'opera':
            driver = webdriver.Opera()
            driver.maximize_window()
            driver.get(cd.url)
            return driver

        else:
            Logger().error('输入浏览器类型不在兼容性范围内')
