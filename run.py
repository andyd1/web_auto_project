# -*- encoding:utf-8 -*-
# 执行接口用例demo
import unittest
import HTMLTestRunner

suite = unittest.TestSuite()
# 方法二
# loader = unittest.TestLoader()
# suit.addTest(loader.loadTestsFromTestCase(test_unittest.TestMathMethod))  #加载类

# 方法三
# loader = unittest.TestLoader()
# suite.addTest(loader.loadTestsFromModule(test_http_request))  # 加载模块

# 执行方法一
# runner = unittest.TextTestRunner()
# runner.run(suit)

# 执行方法二
with open('', 'wb') as file:
    runner = HTMLTestRunner.HTMLTestRunner(stream=file, verbosity=2,
                                           title='接口自动化测试报告', description='andy执行')
    runner.run(suite)
print('python_jenkins')
