# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:base_page.py
# @software:PyCharm
# 封装基本函数 - 执行日志、异常处理、失败截图
# 所有页面的公共部分
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from base.logger import Logger
from base import dir_config as dc
from datetime import datetime


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    # 等待元素可见
    def wait_ele_visible(self, locator, time=30, poll_frequency=0.5, doc=''):
        """
        :param locator:
        :param time:
        :param poll_frequency:
        :param doc:
        :return:
        """
        Logger().info('等待元素{0}可见'.format(locator))
        try:
            # 等待的开始时间
            start = datetime.now()
            WebDriverWait(self.driver, time, poll_frequency).until(ec.visibility_of_element_located(locator))
            # 等待的结束时间
            end = datetime.now()
            # 等待了多久
            time_dif = start - end
            Logger().info('等待时长为{0}'.format(time_dif))
        except Exception as e:
            Logger().error('等待元素{0}可见失败！！！'.format(locator))
            self.save_screenshots(doc)
            raise e

    # 等待元素存在
    def wait_ele_presence(self):
        pass

    # 查找元素
    def get_element(self, locator, doc=''):
        Logger().info("查找元素{0}".format(locator))
        try:
            return self.driver.find_element(*locator)
        except Exception as e:
            Logger().error('查找不对元素{0}'.format(locator))
            self.save_screenshots(doc)
            raise e

    # 点击元素
    def click_element(self, locator, doc=''):
        # 找元素
        ele = self.get_element(locator, doc)
        Logger().info('')
        try:
            ele.click()
        except Exception as e:
            Logger().error('元素{0}点击失败'.format(locator))
            self.save_screenshots(doc)
            raise e

    # 输入操作
    def input_text(self, locator, text, doc=''):
        # 找元素
        ele = self.get_element(locator, doc)
        Logger().info('')
        try:
            ele.send_keys(text)
        except Exception as e:
            Logger().error('元素输入操作失败')
            self.save_screenshots(doc)
            raise e
        pass

    # 获取元素文本内容
    def get_element_text(self, locator, doc=""):
        # 找元素
        ele = self.get_element(locator, doc)
        try:
            return ele.text
        except Exception as e:
            Logger().error('获取元素文本内容失败')
            self.save_screenshots(doc)
            raise e

    # 获取元素属性
    def get_element_attribute(self):
        pass

    # assert处理
    def assert_action(self, action='accept'):
        pass

    # iframe的切换
    def switch_iframe(self, iframe_reference):
        pass

    # 上传操作
    def upload_file(self, file):
        pass

    # 滚动条处理
    # 窗口切换

    # 截图
    def save_screenshots(self, name):
        # 图片名称：模块名称_页面名称_操作名称_年-月-日-时分秒.png
        time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        file_name = dc.screen_path + '/{0}_{1}'.format(name, time) + '.png'
        self.driver.save_screenshot(file_name)
