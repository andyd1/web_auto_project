# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:login_page_ele_locator.py
# @software:PyCharm
# Shopee虾皮登录页面元素定位
from selenium.webdriver.common.by import By


class LoginPageLocator:
    # 元素定位
    # 弹出选择英语选项
    language_english = (By.XPATH, '//*[contains(@class,"shopee-button-outline") and contains(text(),"English")]')
    # 用户名输入框
    username_input = (By.XPATH, '//*[@name="loginKey"]')
    # 密码输入框
    password_input = (By.XPATH, '//*[@name="password"]')
    # 登录按钮
    login_bt = (By.XPATH, '//*[contains(@class,"_1ruZ5a")and contains(text(),"Log In")]')
    # 账号和密码不匹配弹出的错误提示
    message_err = (By.XPATH, '//*[@class="_3mi2mp"]')
    # 忘记密码入口
    forgot_pw = (By.LINK_TEXT, 'Forgot Password')
    # 以手机号码登录入口
    register_bt = (By.LINK_TEXT, 'Log In with Phone Number')
    # 注册入口按钮
    forget_pw_bt = (By.XPATH, 'Sign Up')
