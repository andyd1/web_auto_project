# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:dir_config.py
# @software:PyCharm
# 所有路径的获取配置
import os
from datetime import datetime

current_time = datetime.now().strftime('%Y_%m_%d_%H%M%S')
# 日志路径
log_path = os.path.dirname(os.path.dirname(__file__)) + '/results/logs/{0}.txt'.format(current_time)
print(log_path)
# 截图路径
screen_path = os.path.dirname(os.path.dirname(__file__)) + '/results/screenshots/'
# html报告路径
report_path = os.path.dirname(os.path.dirname(__file__)) + '/results/reports/'
# allure报告路径
allure_report_path = os.path.dirname(os.path.dirname(__file__)) + '/results/allure_reports/'
