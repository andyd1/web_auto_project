# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:logger.py
# @software:PyCharm
# 日志处理
import logging
from base import dir_config as dc


class Logger:

    @staticmethod
    def logger(message, level):
        # logger是收集信息
        # handler是输出信息

        my_logger = logging.getLogger()
        my_logger.setLevel("DEBUG")
        formatter = logging.Formatter('%(asctime)s-%(levelname)s-%(filename)s-%(name)s-日志信息：%(message)s')

        # 创建输出渠道
        my_handler = logging.StreamHandler()  # 控制台
        my_logger.setLevel('DEBUG')
        my_logger.addHandler(my_handler)

        my_file_handler = logging.FileHandler(dc.log_path, encoding='utf-8')  # 文件输出
        my_file_handler.setLevel('DEBUG')
        my_file_handler.setFormatter(formatter)
        my_logger.addHandler(my_file_handler)

        # 收集信息
        if level.upper() == "DEBUG":
            my_logger.setLevel("DEBUG")
            my_file_handler.setLevel('DEBUG')
            my_logger.debug(message)
        elif level.upper() == "INFO":
            my_logger.setLevel("INFO")
            my_file_handler.setLevel('INFO')
            my_logger.info(message)
        elif level.upper() == "WARING":
            my_logger.setLevel("WARING")
            my_file_handler.setLevel('WARING')
            my_logger.warning(message)
        elif level.upper() == "ERROR":
            my_logger.setLevel("ERROR")
            my_file_handler.setLevel('ERROR')
            my_logger.error(message)
        elif level.upper() == "CRITICAL":
            my_logger.setLevel("CRITICAL")
            my_file_handler.setLevel('CRITICAL')
            my_logger.critical(message)
        else:
            print('你输入的错误等级是错误的')
        # 关闭渠道
        my_logger.removeHandler(my_handler)
        my_logger.removeHandler(my_file_handler)

    def debug(self, msg):
        self.logger(msg, 'DEBUG')

    def info(self, msg):
        self.logger(msg, 'INFO')

    def warning(self, msg):
        self.logger(msg, 'WARING')

    def error(self, msg):
        self.logger(msg, 'ERROR')

    def critical(self, msg):
        self.logger(msg, 'CRITICAL')