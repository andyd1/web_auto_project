# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:login_data.py
# @software:PyCharm
# 登录数据
# 登录成功数据
login_success_data = {'username': 'admin', 'password': '123456'}

# 登录失败数据
login_fail_data = [{'username': 'admin', 'password': '123445',
                    'msg': 'Your account has been banned'},
                   {'username': 'root', 'password': '88888',
                    'msg': 'Your account has been banned'}
                   ]
