# _*_ encoding=utf-8 _*_
# @time:2022/2/12 
# Author:赵王发
# @file:test_login.py
# @software:PyCharm
# 登录
from base.logger import Logger
# from datas import login_data as ld
import pytest
from page_objects.login_page_action import LoginPage
import time

@pytest.mark.smoke
@pytest.mark.usefixtures('web_access')
# @pytest.mark.usefixtures('refresh_page')
class TestLogin:

    # 正常用例--登录成功
    # @pytest.mark.smoke   #用pytest打标签来选择执行，通过pytest -m smoke命令行运行
    # def test_success_login(self,access_web):  #fixture的函数名称用来接收它的返回值
    #     Logger().info('===登录用例：正常登录，使用正确的用户名和密码登录===')
    #     # 步骤：输入用户名和密码，点击登录按钮
    #     access_web[1].login_page(ld.login_success_data['name'],ld.login_success_data['password'])
        # 断言：首页中是否找到“退出”这个元素
        # self.assertTrue(IndexPage(access_web[0]).is_exist_logout_ele())
        # assert IndexPage(access_web[0]).is_exist_logout_ele()

    # 输入错误的手机号
    # @pytest.mark.smoke  #用pytest打标签来选择执行，通过pytest -m smoke命令行运行

    def test_fail_login(self, web_access):
        lp = LoginPage(web_access[0])
        for item in web_access[1]:
            Logger().info('===登录用例：账号或密码错误登录===')
            #     print(item)
        # # 步骤：输入用户名和密码，点击登录按钮
        # lp_1 = web_access[1]
        # print('----------')
        # print(lp)
        # print('+++++++++')
        # print(lp_1)
            time.sleep(10)
            lp.click_open_alert()
            time.sleep(2)
            lp.login_page(item['username'], item['password'])
            time.sleep(10)
        # # self.assertEqual(self.lp.get_err_mg_from_lg_area(),item['check'])
            err_mg = lp.get_err_mg_from_lg_area()
            assert err_mg == item['msg']
#
#     # 输入错误的密码
#     @pytest.mark.parametrize('item',ld.error_pw_data)
#     def test_err_pw_login(self,item,access_web):
#         # 步骤：输入用户名和密码，点击登录按钮
#         lp = access_web[1]
#         lp.login_page(item['name'], item['password'])
#         assert lp.get_err_mg_from_lg_area() == item['check']
#

